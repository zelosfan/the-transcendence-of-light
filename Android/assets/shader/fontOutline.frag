uniform sampler2D u_texture;

varying vec4 v_color;
varying vec2 v_texCoord;

const float smoothing = 1.0/16.0;

void main() {
    float distance = texture2D(u_texture, v_texCoord).a;
    float alpha = smoothstep(0.5 - smoothing, 0.5 + smoothing, distance);
    if ((distance > 0.25) && (distance  < 0.5)) {
		alpha = smoothstep(0.25, 0.5, distance);
		gl_FragColor = vec4(1, 1, 1, alpha);
	} else {
		if (distance >= 0.5 && distance <= 0.6) {
			float trans = smoothstep(0.5, 0.6, distance); 
			gl_FragColor = vec4(1 - 1 * trans,1 - 1 * trans,1 - 1 * trans, alpha);
		} else {
			gl_FragColor = vec4(v_color.rgb, alpha);
		}
	}
}
package ggj;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 20:48
 */
class DesktopStarter {

    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "The Transcendence of Light";

        cfg.width = 1024;
        cfg.height = 768;

        cfg.useGL20 = true;
        cfg.resizable = false;

        new LwjglApplication(new Main(), cfg);
    }
}
package ggj.collision;

import com.badlogic.gdx.physics.box2d.Filter;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 22:48
 */
public interface Collisionable {
    Filter getFilter();
}

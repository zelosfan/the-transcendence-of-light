package ggj.collision;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import de.zelosfan.framework.GameObject.GameObject;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 22:47
 */
public class CollisionHandler implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        if (contact.getFixtureA() != null && contact.getFixtureB() != null) {
            if (contact.getFixtureA().getBody().getUserData() instanceof GameObject && contact.getFixtureB().getBody().getUserData() instanceof GameObject) {
                ((GameObject) contact.getFixtureA().getBody().getUserData()).onCollide((GameObject) contact.getFixtureB().getBody().getUserData());
                ((GameObject) contact.getFixtureB().getBody().getUserData()).onCollide((GameObject) contact.getFixtureA().getBody().getUserData());
            }
        }
    }

    @Override
    public void endContact(Contact contact) {
        if (contact.getFixtureA() != null && contact.getFixtureB() != null) {
            if (contact.getFixtureA().getBody().getUserData() instanceof GameObject && contact.getFixtureB().getBody().getUserData() instanceof GameObject) {
                ((GameObject) contact.getFixtureA().getBody().getUserData()).onCollisionLeave((GameObject) contact.getFixtureB().getBody().getUserData());
                ((GameObject) contact.getFixtureB().getBody().getUserData()).onCollisionLeave((GameObject) contact.getFixtureA().getBody().getUserData());
            }
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

     //   if ((contact.getFixtureA().getBody().getUserData() instanceof Player && contact.getFixtureB().getBody().getUserData() instanceof Projectile) || (contact.getFixtureB().getBody().getUserData() instanceof Player && contact.getFixtureA().getBody().getUserData() instanceof Projectile)) {
       //     contact.setEnabled(false);
        //    return;
       // }

        if (contact.getFixtureA().getBody().getUserData() instanceof Collisionable && contact.getFixtureB().getBody().getUserData() instanceof Collisionable) {
            Collisionable a = (Collisionable) contact.getFixtureA().getBody().getUserData();
            Collisionable b = (Collisionable) contact.getFixtureB().getBody().getUserData();

            if ((b.getFilter().maskBits & a.getFilter().categoryBits) == a.getFilter().categoryBits) {
                if ((a.getFilter().maskBits & b.getFilter().categoryBits) == b.getFilter().categoryBits) {
                } else{
                    contact.setEnabled(false);
                }
            } else {
                contact.setEnabled(false);
            }
        }

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}

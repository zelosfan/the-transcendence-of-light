package ggj.collision;

import com.badlogic.gdx.physics.box2d.Fixture;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 22:51
 */
public class DoesCollide implements com.badlogic.gdx.physics.box2d.ContactFilter {
    @Override
    public boolean shouldCollide(Fixture fixtureA, Fixture fixtureB) {
        if (fixtureA.getBody().getUserData() instanceof Collisionable && fixtureB.getBody().getUserData() instanceof Collisionable) {
            Collisionable a = (Collisionable) fixtureA.getBody().getUserData();
            Collisionable b = (Collisionable) fixtureB.getBody().getUserData();

            if ((b.getFilter().maskBits & a.getFilter().categoryBits) == a.getFilter().categoryBits) {
                if ((a.getFilter().maskBits & b.getFilter().categoryBits) == b.getFilter().categoryBits) {
                    return true;
                }
            }

            return false;
        }

        return true;
    }
}
package ggj;

import box2dLight.Light;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.framework.Audio.AudioManager;
import de.zelosfan.framework.GameState.GameStateManager;
import de.zelosfan.framework.IO.ResourceLoader;
import de.zelosfan.framework.Logging.LogManager;
import de.zelosfan.framework.Physics.LightManager;
import de.zelosfan.framework.Physics.PhysicsManager;
import de.zelosfan.framework.Rendering.CameraManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.framework.Rendering.Rendermode;
import de.zelosfan.framework.Timing.GametimeManager;
import ggj.collision.CollisionHandler;
import ggj.collision.DoesCollide;
import ggj.entity.LightColors;
import ggj.entity.Player;
import ggj.gamestate.Menu;
import ggj.gamestate.Playing;
import ggj.world.World;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 20:50
 */
public class Main implements ApplicationListener {

    public static ObjectMap<String, ShaderProgram> shaderProgramObjectMap = new ObjectMap<>();
    public static ObjectMap<String, TextureRegion> textures = new ObjectMap<>();
    public static ObjectMap<String, Sound> soundObjectMap = new ObjectMap<>();
    public static ObjectMap<String, Integer[][]> integerObjectMap = new ObjectMap<>();

    public static Rendermanager rendermanager;
    public static ResourceLoader resourceLoader;
    public static GameStateManager gameStateManager;
    public static AudioManager audioManager;
    public static GametimeManager gametimeManager;
    public static CameraManager cameraManager;

    public static PhysicsManager physicsManager;
    public static LightManager lightManager;

    public static ObjectMap<String, ObjectMap<Integer, BitmapFont>> fontMap = new ObjectMap<>();


    public static int level = 1;

    public static Game game;

    public static boolean nextLevel = false;

    @Override
    public void create() {
        resourceLoader = new ResourceLoader();
        physicsManager = new PhysicsManager(new Vector2(0f, -12f), new CollisionHandler(), 1f / (Gdx.graphics.getWidth() / Gdx.graphics.getPpcX()), 1f / 100);
        PhysicsManager.world.setContactFilter(new DoesCollide());

        rendermanager = new Rendermanager(Rendermode.WORLD, 1024, 768, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), shaderProgramObjectMap);
        gametimeManager = new GametimeManager(100);

        resourceLoader.loadImagePackIntoTextureRegions("./tex.pack", textures);
        resourceLoader.loadImagePackIntoPixelInteger("./map.pack", integerObjectMap);
        resourceLoader.loadFontSizes("./font.pack", fontMap);
        resourceLoader.loadDirectoryFiles("sfx/", soundObjectMap, Sound.class);
        resourceLoader.finishLoading();

        ResourceLoader.loadShaders("./shader/", shaderProgramObjectMap);

       // audioManager.getMusicFiles("./music/");
        audioManager = new AudioManager(0.1f, 0.5f, false, false, soundObjectMap);
        audioManager.getMusicFiles("./music/");
        cameraManager = new CameraManager(rendermanager.camera);
        cameraManager.addChildCam(PhysicsManager.physicsCamera, (1f / Gdx.graphics.getWidth()) * PhysicsManager.GAME_TO_BOX2D_X, (1f / Gdx.graphics.getHeight()) * PhysicsManager.GAME_TO_BOX2D_Y);


        lightManager = new LightManager(PhysicsManager.world, PhysicsManager.physicsCamera);
        lightManager.rayHandler.setAmbientLight(0, 0, 0, 0);
        lightManager.rayHandler.setShadows(true);

        Filter filter = new Filter();
        filter.categoryBits = LightColors.NORMAL.v();
        filter.maskBits = (short) (LightColors.NORMAL.v() | LightColors.ALWAYS.v());
        Light.setContactFilter(filter);

        gameStateManager = new GameStateManager();
        game = new Game();


      // gameStateManager.setCurrentGameState(new Playing(gameStateManager, true));
        gameStateManager.setCurrentGameState(new Menu(gameStateManager, true));
    }


    @Override
    public void resize(int i, int i2) {

    }

    public void tick() {
        Main.gameStateManager.tick();
        audioManager.tick();
        if (nextLevel) {
            nextLevel();
            nextLevel = false;
            Main.audioManager.playSound("nextLevel.wav");
        }
    }

    public static void nextLevel() {
        level++;
        if (Main.level >= Main.integerObjectMap.size) {
            Main.level = 1;
            Main.gameStateManager.setCurrentGameState(new Menu(Main.gameStateManager, true));
        } else {
            restart();
        }
    }

    public static void lose() {
        Main.audioManager.playSound("death.wav");
        restart();
    }

    public static void restart() {
        Vector2 pm = game.player.walkingDir.cpy();

        game.dispose();
        game = new Game();
        game.player.walkingDir = pm;
    }

    @Override
    public void render() {
        gametimeManager.updateTime(Gdx.graphics.getDeltaTime());

        while(gametimeManager.tick()) {
            tick();
        }

        rendermanager.render(gameStateManager);
        lightManager.updateAndRender();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        resourceLoader.dispose();
        gametimeManager.dispose();
        audioManager.dispose();
    }
}

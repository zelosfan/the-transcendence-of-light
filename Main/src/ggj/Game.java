package ggj;

import com.badlogic.gdx.utils.Disposable;
import de.zelosfan.framework.Physics.PhysicsManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import ggj.entity.Block;
import ggj.entity.Player;
import ggj.world.World;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 21:29
 */
public class Game implements Disposable{
    public World world;
    public Player player;

    public Game() {
            world = new World("map" + Main.level);
            player = new Player(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * world.playerSpawn.x  , PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * world.playerSpawn.y, null, null);

            world.addEntity(player);
    }

    @Override
    public void dispose() {
        world.dispose();
    }

    public void tick() {
        world.tick();

        if (player.getY() < -5) {
            Main.lose();
        }
    }

    public void render(Rendermanager rendermanager) {
        world.render(rendermanager);
    }
}

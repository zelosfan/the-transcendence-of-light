package ggj.gamestate;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import de.zelosfan.framework.GameState.GameState;
import de.zelosfan.framework.GameState.GameStateManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import ggj.Main;
import ggj.gamestate.instaces.World;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 21:18
 */
public class Playing extends GameState{
    World world;

    public Playing(GameStateManager gameStateManager, boolean _inputPreProcessor) {
        super(gameStateManager, _inputPreProcessor);
        world = new World();
    }

    @Override
    protected InputMultiplexer buildInputMultiplexer() {
        return new InputMultiplexer(world);
    }

    @Override
    public void onActivate() {
        world.onActivate();
    }

    @Override
    public void onDeactivate() {
        world.onDeactivate();
    }

    @Override
    public void tick() {
        Main.game.tick();
        world.tick();

    }

    @Override
    public void render(Rendermanager rendermanager) {
        super.render(rendermanager);

        world.render(rendermanager);
    }
}

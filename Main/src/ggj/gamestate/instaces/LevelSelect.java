package ggj.gamestate.instaces;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import de.zelosfan.framework.GameState.Instance;
import de.zelosfan.framework.Rendering.Rendermanager;
import ggj.Game;
import ggj.Main;
import ggj.gamestate.Playing;
import ggj.world.World;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 18:41
 */
public class LevelSelect implements Instance {

    public int selected = 0;


    @Override
    public void onActivate() {
        Main.game.world.dispose();
        Main.game.world = new World("mapx");
      //  table.add(new Label("blablablalbalbal", Main.skin, "tektonpro"));
    }

    @Override
    public void onDeactivate() {

    }

    @Override
    public void render(Rendermanager rendermanager) {

    //    Main.lightManager.updateAndRender();
        Main.rendermanager.drawIndependentText(Main.fontMap.get("tektonpro").get(32), "font", "Level Selection", 0.42f, 0.8f, Color.WHITE);

        for (int i = 0; i < Main.integerObjectMap.size - 1; i++) {
            if (i == selected) {
                Main.rendermanager.drawIndependent(Main.textures.get("pdummy"), 0.42f, 0.552f - 0.07f * i, 0.03f, 1, 0);
                Main.rendermanager.drawIndependentText(Main.fontMap.get("tektonpro").get(28), "font", "Level " + (i + 1), 0.46f, 0.6f - 0.07f * i, Color.YELLOW);
            } else {
                Main.rendermanager.drawIndependentText(Main.fontMap.get("tektonpro").get(28), "font",  "Level " + (i + 1), 0.46f, 0.6f - 0.07f * i, Color.WHITE);
            }
        }

        Main.game.world.render(rendermanager);
    }

    @Override
    public void tick() {
        Main.game.world.tick();
        Main.physicsManager.tick();
        //Main.cameraManager.setMain(Main.game.player.getX() * PhysicsManager.BOX2D_TO_GAME_X * Gdx.graphics.getWidth(), Main.game.player.getY()  * PhysicsManager.BOX2D_TO_GAME_Y * Gdx.graphics.getHeight());
        Main.cameraManager.setMain(600, 600);

    }

    @Override
    public boolean keyDown(int i) {
        switch (i) {
            case Input.Keys.W:
                selected--;
                if (selected < 0) {
                    selected = Main.integerObjectMap.size - 2;
                }
                Main.audioManager.playSound("hitNothing.wav");
                break;

            case Input.Keys.S:
                selected++;
                if (selected > Main.integerObjectMap.size - 2) {
                    selected = 0;
                }
                Main.audioManager.playSound("hitNothing.wav");
                break;

            case Input.Keys.SPACE:
                Main.audioManager.playSound("jump.wav");
                Main.level = selected + 1;
                Main.game.dispose();
                Main.game = new Game();

                Main.gameStateManager.setCurrentGameState(new Playing(Main.gameStateManager, true));
        }

        return false;
    }

    @Override
    public boolean keyUp(int i) {
        return false;
    }

    @Override
    public boolean keyTyped(char c) {
        return false;
    }

    @Override
    public boolean touchDown(int i, int i2, int i3, int i4) {
        return false;
    }

    @Override
    public boolean touchUp(int i, int i2, int i3, int i4) {
        return false;
    }

    @Override
    public boolean touchDragged(int i, int i2, int i3) {
        return false;
    }

    @Override
    public boolean mouseMoved(int i, int i2) {
        return false;
    }

    @Override
    public boolean scrolled(int i) {
        return false;
    }
}

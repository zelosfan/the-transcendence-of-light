package ggj.gamestate.instaces;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import de.zelosfan.framework.GameState.Instance;
import de.zelosfan.framework.Physics.PhysicsManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import ggj.Main;
import ggj.gamestate.Menu;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 21:19
 */
public class World implements Instance {
    @Override
    public void onActivate() {

    }

    @Override
    public void onDeactivate() {

    }

    @Override
    public void render(Rendermanager rendermanager) {
        if (Main.level == 1) {
            rendermanager.drawfh(Main.textures.get("movement"), 0.14f, 1.1f, 0.24f, 0.5f);
            rendermanager.drawfh(Main.textures.get("controls"), 0.4f, 1.1f, 0.14f, 1f);
        }

        Main.game.render(rendermanager);
     //   Main.lightManager.updateAndRender();


    }

    @Override
    public void tick() {
        Main.cameraManager.setMain(Main.game.player.getX() * PhysicsManager.BOX2D_TO_GAME_X * Gdx.graphics.getWidth(), Main.game.player.getY()  * PhysicsManager.BOX2D_TO_GAME_Y * Gdx.graphics.getHeight());
        Main.physicsManager.tick();
        float accelY = Gdx.input.getAccelerometerY();

        float treshold = 3;

        //System.out.println("" + accelY);

        if (accelY > treshold) {
            //Main.game.player.walkingDir.x += 1;
        } else if (accelY <= treshold && accelY >= -treshold) {
           // Main.game.player.walkingDir.x = 0;
        } else {
           // Main.game.player.walkingDir.x -= 1;
        }
    }

    @Override
    public boolean keyDown(int i) {

        switch (i) {
            case Input.Keys.D:
                Main.game.player.walkingDir.x += 1;
                return true;

            case Input.Keys.A:
                Main.game.player.walkingDir.x -= 1;
                return true;

            case Input.Keys.SPACE:
                Main.game.player.walkingDir.y = 1;
                return true;

            case Input.Keys.F1:
                Main.restart();
                return true;

            /*case Input.Keys.F2:
                Main.nextLevel = true;
                return true;
*/
            case Input.Keys.ESCAPE:
                LevelSelect levelSelect = new LevelSelect();
                levelSelect.selected = Main.level - 1;
                Main.gameStateManager.setCurrentGameState(new Menu(Main.gameStateManager, true, levelSelect));
                return true;
        }

        return false;
    }

    @Override
    public boolean keyUp(int i) {

        switch (i) {
            case Input.Keys.D:
                //if (Main.game.player.walkingDir.x != 0) {
                    Main.game.player.walkingDir.x -= 1;
               // }

                return true;

            case Input.Keys.A:
                //if (Main.game.player.walkingDir.x != 0) {
                    Main.game.player.walkingDir.x += 1;
                //}
                return true;
        }

        return false;
    }

    @Override
    public boolean keyTyped(char c) {
        return false;
    }

    @Override
    public boolean touchDown(int i, int i2, int i3, int i4) {

        return false;
    }

    @Override
    public boolean touchUp(int i, int i2, int i3, int i4) {
        return false;
    }

    @Override
    public boolean touchDragged(int i, int i2, int i3) {
        return false;
    }

    @Override
    public boolean mouseMoved(int i, int i2) {
        return false;
    }

    @Override
    public boolean scrolled(int i) {
        return false;
    }

}

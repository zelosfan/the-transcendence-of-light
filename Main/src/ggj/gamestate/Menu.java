package ggj.gamestate;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import de.zelosfan.framework.GameState.GameState;
import de.zelosfan.framework.GameState.GameStateManager;
import de.zelosfan.framework.GameState.Instance;
import de.zelosfan.framework.Rendering.Rendermanager;
import ggj.Main;
import ggj.gamestate.instaces.MainMenu;
import ggj.gamestate.instaces.World;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 21:18
 */
public class Menu extends GameState{

    public Instance menuInstance;

    public Menu(GameStateManager gameStateManager, boolean _inputPreProcessor) {
        super(gameStateManager, _inputPreProcessor);

        menuInstance = new MainMenu();
    }

    public Menu(GameStateManager gameStateManager, boolean _inputPreProcessor, Instance menu) {
        super(gameStateManager, _inputPreProcessor);

        menuInstance = menu;
    }

    public void assignNewMenu(Instance _menuInstance) {
        menuInstance = _menuInstance;
        Gdx.input.setInputProcessor(buildInputMultiplexer());
    }

    @Override
    protected InputMultiplexer buildInputMultiplexer() {
        return new InputMultiplexer(menuInstance);
    }

    @Override
    public void onActivate() {
        menuInstance.onActivate();
    }

    @Override
    public void onDeactivate() {
        menuInstance.onDeactivate();
    }

    @Override
    public void tick() {
        menuInstance.tick();
    }

    @Override
    public void render(Rendermanager rendermanager) {
        super.render(rendermanager);

        menuInstance.render(rendermanager);
    }
}

package ggj.entity;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import de.zelosfan.framework.Physics.PhysicsManager;
import ggj.Main;
import ggj.entity.blocks.BlinkingBlock;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 23:43
 */
public class Block extends Entity {

    public static float[] vertices;
    public static PolygonShape polygonShape;

    public static float SIZE = 0.05f;

    static {
        vertices = new float[16];
        vertices[0] = 0.15f;
        vertices[1] = 0f;
        vertices[2] = 0;
        vertices[3] = 0.15f;
        vertices[4] = 0f;
        vertices[5] = 0.85f;
        vertices[6] = 0.15f;
        vertices[7] = 1f;
        vertices[8] = 0.85f;
        vertices[9] = 1f;
        vertices[10] = 1f;
        vertices[11] = 0.85f;
        vertices[12] = 1f;
        vertices[13] = 0.15f;
        vertices[14] = 0.85f;
        vertices[15] = 0f;

        polygonShape = new PolygonShape();

        for (int i = 0; i < vertices.length; i++) {
            vertices[i] *= SIZE * PhysicsManager.GAME_TO_BOX2D_X;
        }
        polygonShape.set(vertices);
    }



    public Block(float _x, float _y, float density, float friction, float restitution) {
        super(_x, _y, SIZE, 1, 0, BodyDef.BodyType.StaticBody, true, PhysicsManager.world, density, friction, restitution, false, null, polygonShape);
    }

    @Override
    public TextureRegion getTexture() {
        return BlinkingBlock.empty;
    }

    @Override
    public void updateFilter() {

        filter.categoryBits = LightColors.NORMAL.v();
        filter.maskBits = LightColors.NORMAL.v();
    }
}

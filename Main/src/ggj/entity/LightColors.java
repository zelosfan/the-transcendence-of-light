package ggj.entity;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 07:06
 */
public enum LightColors {
        NORMAL((short) 1),
        BLUE((short) 2),
        RED((short) 4),
        ALWAYS((short) 8)
      //  PLAYER((short) 8),
      //  ITEM_PICKUP((short) 16),
      //  ITEM_THROWN((short) 32),
     //   PLACEABLE((short) 64)
        ;

        private Short value;

        private LightColors(Short value) {
            this.value = value;
        }

        public short v() {
            return this.value;
        }

}

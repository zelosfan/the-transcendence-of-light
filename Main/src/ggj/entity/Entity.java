package ggj.entity;

import box2dLight.Light;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import de.zelosfan.framework.GameObject.GameObject;
import ggj.collision.Collisionable;

import java.util.LinkedList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 00:15
 */
public abstract class Entity extends GameObject implements Collisionable {
    public Filter filter;

    public Entity(float _x, float _y, float _sizeX, float _sizeY, float _rotation, BodyDef.BodyType _bodyType, boolean _fixedRotation, World _world, float density, float friction, float restitution, boolean isSensor, float[] shapeVertices, PolygonShape shape) {
        super(_x, _y, _sizeX, _sizeY, _rotation, _bodyType, _fixedRotation, _world, density, friction, restitution, isSensor, shapeVertices, shape);
        filter = new Filter();
        updateFilter();
    }

    public boolean lightAttachAvailable() {
        if (lights == null) lights = new LinkedList<>();
        return lights.size() < 1;
    }

    public boolean lightDeAttachAvailable() {
        if (lights == null) return false;
        return lights.size() == 1;
    }

    public Light detachLight() {
        Light light = lights.removeFirst();
        updateFilter();
        return light;
    }

    @Override
    public void attachLight(Light light) {
        super.attachLight(light);
        updateFilter();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public abstract void updateFilter();

}

package ggj.entity.blocks;

import box2dLight.PointLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.framework.GameObject.GameObject;
import ggj.Main;
import ggj.entity.Block;
import ggj.entity.Player;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 08:40
 */
public class NextLevelBlock extends ShownBlock {
    public static TextureRegion goal = Main.textures.get("goal");

    public NextLevelBlock(float _x, float _y, float density, float friction, float restitution) {
        super(_x, _y, density, friction, restitution);

        attachLight(new PointLight(Main.lightManager.rayHandler, 6, Color.YELLOW, 3, Block.SIZE * 0.5f, Block.SIZE * 0.5f), 0.5f, 0.5f);
    }

    @Override
    public void onCollide(GameObject o) {
        super.onCollide(o);

        if (o instanceof Player) {
            Main.nextLevel = true;
        }

    }

    @Override
    public TextureRegion getTexture() {
        return goal;
    }
}

package ggj.entity.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.framework.GameObject.GameObject;
import ggj.Main;
import ggj.entity.Block;
import ggj.entity.Player;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 06:54
 */
public class CatalystBlock extends ShownBlock{
    public static TextureRegion catalyst = Main.textures.get("catalyst");

    public CatalystBlock(float _x, float _y, float density, float friction, float restitution) {
        super(_x, _y, density, friction, restitution);
    }

    @Override
    public void onCollide(GameObject o) {
        super.onCollide(o);

        if (o instanceof Player) {
            if (((Player) o).lightDeAttachAvailable() && this.lightAttachAvailable()) {
                this.attachLight(((Player) o).detachLight(), 0.5f, 0.5f);
                this.lights.getFirst().setDistance(lights.getFirst().getDistance() + 4);
                Main.audioManager.playSound("giveLight.wav");
            } else if (this.lightDeAttachAvailable()) {
                if (((Player) o).lightAttachAvailable()) {
                    o.attachLight(this.detachLight());
                    o.lights.getFirst().setDistance(o.lights.getFirst().getDistance() -4);
                    Main.audioManager.playSound("takeLight.wav");
                }
            } else {
                Main.audioManager.playSound("hitNothing.wav");
            }
        }
    }

    @Override
    public TextureRegion getTexture() {
        return catalyst;
    }
}

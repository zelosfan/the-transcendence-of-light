package ggj.entity.blocks;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.framework.Audio.AudioManager;
import de.zelosfan.framework.GameObject.GameObject;
import ggj.Main;
import ggj.entity.Player;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 13:23
 */
public class BlueMasterBlock extends ShownBlock{
    public static boolean activated = false;
    public static TextureRegion blueMaster = Main.textures.get("blueMaster");

    public BlueMasterBlock(float _x, float _y, float density, float friction, float restitution) {
        super(_x, _y, density, friction, restitution);
    }

    @Override
    public void onCollide(GameObject o) {
        super.onCollide(o);

        if (o instanceof Player) {
            if (((Player) o).lightDeAttachAvailable() && this.lightAttachAvailable()) {
                if (o.lights.getFirst().getColor().equals(Color.BLUE)) {
                    this.attachLight(((Player) o).detachLight(), 0.5f, 0.5f);
                    this.lights.getFirst().setDistance(lights.getFirst().getDistance() + 4);
                    activated = true;
                    Main.audioManager.playSound("giveLight.wav");
                }
            }
        }
    }

    @Override
    public TextureRegion getTexture() {
        return blueMaster;
    }
}

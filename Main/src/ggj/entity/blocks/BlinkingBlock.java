package ggj.entity.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.framework.Timing.GametimeManager;
import ggj.Main;
import ggj.entity.Block;
import ggj.entity.LightColors;

import java.util.LinkedList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 10:29
 */
public class BlinkingBlock extends Block {

    public static TextureRegion empty =Main.textures.get("empty");
    public static TextureRegion block = Main.textures.get("block");

    int interval = 1;
    int startInterval = 1;
    boolean switched = false;

    public BlinkingBlock(float _x, float _y, int blinkInterval, int _startInterval, float density, float friction, float restitution) {
        super(_x, _y, density, friction, restitution);
        interval = blinkInterval;
        startInterval = _startInterval;
    }

    @Override
    public void tick() {
        super.tick();
        if (interval == 0) {
            interval = 1;
        }

        if ((GametimeManager.TICKCOUNT + startInterval) % interval * 2 < interval) {
            if (switched) {
                filter.categoryBits = LightColors.NORMAL.v();
                filter.maskBits = (short) (LightColors.NORMAL.v() | LightColors.ALWAYS.v());
                switched = false;
            }
        } else {
            if (!switched) {
                filter.categoryBits = LightColors.RED.v();
                filter.maskBits = (short) (LightColors.RED.v() |  LightColors.ALWAYS.v());
                switched = true;
            }
        }
    }

    @Override
    public TextureRegion getTexture() {
        if (switched) {
            return empty;
        } else {
            return block;
        }
    }
}

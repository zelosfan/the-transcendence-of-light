package ggj.entity.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import ggj.Main;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 12:20
 */
public class BouncyBlock extends ShownBlock{
    public static TextureRegion bouncy = Main.textures.get("bouncy");

    public BouncyBlock(float _x, float _y, float density, float friction, float restitution) {
        super(_x, _y, density, friction, restitution);
    }

    @Override
    public TextureRegion getTexture() {
        return bouncy;
    }
}

package ggj.entity.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import ggj.Main;
import ggj.entity.Block;
import ggj.entity.LightColors;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 07:19
 */
public class ShownBlock extends Block {

    public ShownBlock(float _x, float _y, float density, float friction, float restitution) {
        super(_x, _y, density, friction, restitution);
    }

    @Override
    public TextureRegion getTexture() {
        return BlinkingBlock.block;
    }

    @Override
    public void updateFilter() {
        super.updateFilter();

        filter.categoryBits = LightColors.ALWAYS.v();
        filter.maskBits = (short) (LightColors.NORMAL.v() | LightColors.ALWAYS.v() | LightColors.BLUE.v() | LightColors.RED.v() );
    }
}

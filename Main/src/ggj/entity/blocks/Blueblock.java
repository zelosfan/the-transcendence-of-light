package ggj.entity.blocks;

import box2dLight.PointLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.framework.Physics.LightManager;
import ggj.Main;
import ggj.entity.Block;
import ggj.entity.LightColors;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 08:11
 */
public class Blueblock extends Block {
    public Blueblock(float _x, float _y, float density, float friction, float restitution) {
        super(_x, _y, density, friction, restitution);
    }

    public boolean activated = false;

    @Override
    public void tick() {
        if (BlueMasterBlock.activated && !activated) {
            updateFilter();
            activated = true;
            attachLight(new PointLight(Main.lightManager.rayHandler, 10, Color.BLUE, 3, 0, 0), 0.5f, 0.5f);
        }
    }

    @Override
    public void updateFilter() {
        if (filter != null) {
            if (BlueMasterBlock.activated) {
                filter.categoryBits = LightColors.ALWAYS.v();
                filter.maskBits = (short) (LightColors.ALWAYS.v() | LightColors.RED.v() | LightColors.NORMAL.v());
            } else {
                filter.categoryBits = LightColors.BLUE.v();
                filter.maskBits = 0;
            }
        }
    }

    @Override
    public TextureRegion getTexture() {
        return BlinkingBlock.block;
    }
}

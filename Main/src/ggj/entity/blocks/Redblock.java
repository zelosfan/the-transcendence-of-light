package ggj.entity.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import ggj.Main;
import ggj.entity.Block;
import ggj.entity.LightColors;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 08:11
 */
public class Redblock extends Block {
    public static TextureRegion redblock = Main.textures.get("redblock");

    public Redblock(float _x, float _y, float density, float friction, float restitution) {
        super(_x, _y, density, friction, restitution);
    }

    @Override
    public void updateFilter() {
        filter.categoryBits = LightColors.RED.v();
        filter.maskBits = (short) (LightColors.RED.v() | LightColors.ALWAYS.v());
    }

    @Override
    public TextureRegion getTexture() {
        return redblock;
    }
}

package ggj.entity.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.framework.GameObject.GameObject;
import ggj.Main;
import ggj.entity.Player;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 06:54
 */
public class LampBlock extends ShownBlock{
    public static TextureRegion light = Main.textures.get("light");

    public LampBlock(float _x, float _y, float density, float friction, float restitution) {
        super(_x, _y, density, friction, restitution);
    }

    @Override
    public void onCollide(GameObject o) {
        Main.audioManager.playSound("hitNothing.wav");
    }

    @Override
    public TextureRegion getTexture() {
        return light;
    }
}

package ggj.entity.blocks;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.framework.GameObject.GameObject;
import ggj.Main;
import ggj.entity.Block;
import ggj.entity.LightColors;
import ggj.entity.Player;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 07:19
 */
public class CrumbleBlock extends Block {

    public static TextureRegion blockCrumble = Main.textures.get("crumble");

    int tickCountdown = 100;
    boolean activated = false;

    public CrumbleBlock(float _x, float _y, float density, float friction, float restitution, int lifetime) {
        super(_x, _y, density, friction, restitution);
        tickCountdown = lifetime;
    }

    @Override
    public TextureRegion getTexture() {
        return blockCrumble;
    }

    @Override
    public void onCollide(GameObject o) {
        super.onCollide(o);

        if (o instanceof Player) {
            activated = true;
        }
    }

    @Override
    public void tick() {
        super.tick();

        if (activated) {
            tickCountdown--;

            if (tickCountdown < 0)  {
                remove = true;
                Main.audioManager.playSound("crumble.wav");
            }
        }
    }



    @Override
    public void updateFilter() {
        super.updateFilter();

        filter.categoryBits = LightColors.ALWAYS.v();
        filter.maskBits = (short) (LightColors.NORMAL.v() | LightColors.ALWAYS.v() | LightColors.BLUE.v() | LightColors.RED.v() );
    }
}

package ggj.entity;

import box2dLight.Light;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import de.zelosfan.framework.GameObject.GameObject;
import de.zelosfan.framework.Physics.PhysicsManager;
import de.zelosfan.framework.Util.MathUtil;
import ggj.Main;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 21:37
 */
public class Player extends Entity{
    public float speed = 9;
    public float acceleration = 0.45f;
    public float accelerationModifier = 1;
    public float jumpAccel = 20;

    public boolean jumpCooldown = false;

    public Vector2 walkingDir = new Vector2(0, 0);
   // public Vector2 lastWalkingDir = walkingDir;

    public Player(float _x, float _y, float[] shapeVertices, PolygonShape shape) {
        super(_x, _y, 0.04f, 1.0f, 0, BodyDef.BodyType.DynamicBody, true, PhysicsManager.world, 120, 0.1f, 0, false, shapeVertices, shape);

    }

    @Override
    public TextureRegion getTexture() {
        return Main.textures.get("pdummy");
    }

    public void tick() {
        if (walkingDir != null) {
            float vacceleration = acceleration * accelerationModifier;
            float desiredVelY = speed * walkingDir.y;
            float desiredVelX = speed * walkingDir.x;

            float impulseSpeedX = 0;
            float impulseSpeedY = 0;

            if (walkingDir.x != 0 || walkingDir.y != 0) {

                if (walkingDir.x != 0 ) {
                    impulseSpeedX = MathUtil.getGradualImpulseForce(desiredVelX, vacceleration * walkingDir.x, getBody().getLinearVelocity().x, getBody().getMass());
                }

                if (walkingDir.y == 1) {
                    if (!jumpCooldown) {
                        impulseSpeedY = MathUtil.getGradualImpulseForce(desiredVelY, vacceleration * jumpAccel, getBody().getLinearVelocity().y, getBody().getMass());
                        walkingDir.y = 0;
                        jumpCooldown = true;
                        Main.audioManager.playSound("jump.wav");
                    } else {
                        walkingDir.y = 0;
                    }
                }
              //  lastWalkingDir = walkingDir.cpy();

                this.applyLinearImpules(impulseSpeedX, impulseSpeedY);
            }

        }

    }

    @Override
    public void onCollide(GameObject o) {
        super.onCollide(o);


        if (o instanceof Block) {
            if (jumpCooldown) {
              //  Main.audioManager.playSound("hitNothing.wav");
            }

            jumpCooldown = false;
        }




    }

    @Override
    public void updateFilter() {
        if (lightAttachAvailable()) {
            filter.categoryBits = LightColors.NORMAL.v();
            filter.maskBits = (short) (LightColors.NORMAL.v() | LightColors.ALWAYS.v());
        } else {
            if (lights.getFirst().getColor().equals(Color.RED)) {
                filter.categoryBits = LightColors.RED.v();
                filter.maskBits = (short) (LightColors.RED.v() | LightColors.ALWAYS.v());
            } else {
                filter.categoryBits = LightColors.NORMAL.v();
                filter.maskBits = (short) (LightColors.NORMAL.v() | LightColors.ALWAYS.v());
            }

        }
     //   Light.setContactFilter(filter);
    }
}

package ggj.world;

import box2dLight.ConeLight;
import box2dLight.Light;
import box2dLight.PointLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import de.zelosfan.framework.GameObject.GameObject;
import de.zelosfan.framework.Physics.PhysicsManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import ggj.Main;
import ggj.entity.Block;
import ggj.entity.Entity;
import ggj.entity.blocks.*;

import java.util.ArrayList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 24.01.14
 * Time: 21:27
 */
public class World implements Disposable {

    public ArrayList<GameObject> entities = new ArrayList<>();
    Integer[][] intmap;
    public Vector2 playerSpawn;

    public World(Integer[][] _intmap)  {
        intmap = _intmap;

        for (int x = 0; x < intmap.length; x++) {
            for (int y = 0; y < intmap[x].length; y++) {
                switch (intmap[x][y]) {
                    case -1:
                        break;

                    case -1778420225:
                        playerSpawn = new Vector2(x,  (intmap[x].length - y));
                        break;

                    case 255:
                        entities.add(new ShownBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0.3f, 0));
                        break;

                    case  -16772353:
                        entities.add(new BouncyBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0.1f, 0.9f));
                        break;

                    case 931165183:
                        entities.add(new BlinkingBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 300, 0, 1f, 0.1f, 0));
                        break;

                    case 153954303:
                        entities.add(new BlinkingBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 300, 150, 1f, 0.1f, 0));
                        break;

                    case -39386881:
                        Light lightC = new ConeLight(Main.lightManager.rayHandler, 40, Color.YELLOW, 15, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), -95, 65);
                        break;

                    case 1728806655:
                        CatalystBlock catalystBlock2 = new CatalystBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0, 0.9f);

                        catalystBlock2.attachLight(new PointLight(Main.lightManager.rayHandler, 40, Color.RED, 16, 0, 0), 0.5f, 0.5f);
                        entities.add(catalystBlock2);
                        break;

                    case -453050113:
                        LampBlock lampBlock = new LampBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0, 0);
                        lampBlock.attachLight(new PointLight(Main.lightManager.rayHandler, 40), 0.5f, 0.5f);
                        entities.add(lampBlock);
                        break;

                    case 1398731263:
                        entities.add(new BlueMasterBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0.1f, 0));
                        break;

                    case 50874111:
                        CatalystBlock catalystBlockBlue =new CatalystBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0, 0);

                        catalystBlockBlue.attachLight(new PointLight(Main.lightManager.rayHandler, 60, Color.BLUE, 9, 0, 0), 0.5f, 0.5f);
                        entities.add(catalystBlockBlue);
                        break;

                    case 1834572287:
                        entities.add(new Block(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0, 0));
                        break;

                    case 16773375:
                        entities.add(new CatalystBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0, 0));
                        break;

                    case 1175807:
                        CatalystBlock catalystBlock =new CatalystBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0, 0);

                        catalystBlock.attachLight(new PointLight(Main.lightManager.rayHandler, 80, Color.WHITE, 11, 0, 0), 0.5f, 0.5f);
                        entities.add(catalystBlock);
                        break;

                    case 88474879:
                        entities.add(new Blueblock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0, 0));
                        break;

                    case -16758529:
                        CatalystBlock bouncyRed =new CatalystBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0, 0);

                        bouncyRed.attachLight(new PointLight(Main.lightManager.rayHandler, 80, Color.RED, 7, 0, 0), 0.5f, 0.5f);
                        entities.add(bouncyRed);

                        break;

                    case -369033217:
                        entities.add(new Redblock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0, 0));
                        break;

                    case 16320255:
                        entities.add(new NextLevelBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0, 0));
                        break;

                    case 654838015:
                        entities.add(new CrumbleBlock(PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * x, PhysicsManager.GAME_TO_BOX2D_X * Block.SIZE * (intmap[x].length - y), 0, 0, 0, 40));
                        break;


                    default:
                        System.out.println(x + " - " + (intmap[x].length - y) + " - " + intmap[x][y] );
                        break;
                }

            }
        }
    }

    public World(String map) {
        this(Main.integerObjectMap.get(map));
    }

    public void addEntity(GameObject gameObject) {
        entities.add(gameObject);
    }

    public void tick() {
        ArrayList<GameObject> dumpList = new ArrayList<>();
        for (GameObject gameObject: entities) {
            gameObject.tick();

            if (gameObject.remove) {
                dumpList.add(gameObject);
            }
        }

        for (GameObject gameObject: dumpList) {
            entities.remove(gameObject);
            gameObject.dispose();
        }

    }

    public void render(Rendermanager rendermanager) {
        for (GameObject gameObject: entities) {
            rendermanager.render(gameObject);
        }
    }

    @Override
    public void dispose() {
        ArrayList<GameObject> dumpList = (ArrayList<GameObject>) entities.clone();

        for (GameObject gameObject: dumpList) {
            entities.remove(gameObject);
            gameObject.dispose();
        }

    }
}

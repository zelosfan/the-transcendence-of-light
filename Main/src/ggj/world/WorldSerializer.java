package ggj.world;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.01.14
 * Time: 03:05
 */
public class WorldSerializer extends Serializer<World> {
    @Override
    public void write(Kryo kryo, Output output, World world) {
        output.writeInt(world.intmap.length);
        output.writeInt(world.intmap[0].length);
        System.out.println(world.intmap.length+ " " + world.intmap[0].length);
        for (int x = 0; x < world.intmap.length; x++) {
            for (int y = 0; y < world.intmap[0].length; y++) {
                output.writeInt(world.intmap[x][y]);
            }
        }
    }

    @Override
    public World read(Kryo kryo, Input input, Class<World> worldClass) {
        int lX = input.readInt();
        int lY = input.readInt();

        Integer[][] integers = new Integer[lX][lY];
        System.out.println(lX + " " + lY);
        for (int x = 0; x < lX; x++) {
            for (int y = 0; y < lY; y++) {
                integers[x][y] = input.readInt();
            }
        }

        return new World(integers);
    }
}
